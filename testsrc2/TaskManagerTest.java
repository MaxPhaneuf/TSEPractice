import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;

public class TaskManagerTest {
	
	private TaskManager classUnderTest;
	
	@Before
	public void setUp() throws Exception 
	{
		classUnderTest = new TaskManager();
	}

	@Test
	public void testCreateTask_sizeNotEmpty() {
		classUnderTest.createTask("name", "descr", 1);
		assertNotEquals(0, classUnderTest.getTaskList().size());
	}

	@Test
	public void testCreateTask_nameEquals() {
		classUnderTest.createTask("name", "descr", 1);
		assertEquals("name", classUnderTest.getTaskList().get(0).getName());
	}
	
	@Test
	public void testCreateTask_addMultipleTest(){
		for (int i = 0; i < 10; i++) 
		{
			classUnderTest.createTask("name", "descr", i);
			assertNotEquals(i, classUnderTest.getTaskList().size());
		}
	}
}
