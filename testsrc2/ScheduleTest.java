import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Test;

public class ScheduleTest {

	@Test
	public void scheduleDateAssign() {
		Schedule test = new Schedule();
		assert(test.date.toString().equals(LocalDate.now().toString()));
	}
	
	@Test
	public void scheduleTaskListAssign() {
		ArrayList<AssignedTask> taskList = new ArrayList<AssignedTask>();
		taskList.add(new AssignedTask());
		Schedule test = new Schedule(taskList);
		assert(test.taskList.size() > 0);
						
	}

}
