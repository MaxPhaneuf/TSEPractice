import java.util.ArrayList;
import java.util.Scanner;

public class TaskManager {
	private ArrayList<Task> taskList = new ArrayList<Task>();
	
	
	public void scanTaskInfo()
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Choose a task name :");
		String name = sc.nextLine();
		
		System.out.println("Choose a task description :");
		String description = sc.nextLine();

		System.out.println("Choose a task difficulty (0 to 10) :");
		int difficulty = scanDifficulty(sc.nextLine());
		
		createTask(name,description,difficulty);
	}
	
	int scanDifficulty(String scan)
	{
		int diff = 0;
		try
		{
			diff = Integer.parseInt(scan);
		}
		catch(NumberFormatException e)
		{
			System.out.println("You must enter a number, the difficulty will be set 0");
		}
		return diff;
	}
	
	void createTask(String name, String description, int difficulty)
	{
		Task task = new Task(name,description,difficulty);
		taskList.add(task);
	}
	
	public ArrayList<Task> getTaskList()
	{
		return taskList;
	}
}
