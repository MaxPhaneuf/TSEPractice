
public class Task {
	public Task() {
	
	}
	
	public Task (String name, String description, int difficulty)
	{
		this.name = name;
		this.description = description;
		this.difficulty = difficulty;
	}
 	String name;
	String description;
	int difficulty;
	
	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}
	
	public int getDifficulty() {
		return difficulty;
	}

	@Override
	public String toString() {
		return "Task [name=" + name + ", description=" + description + ", difficulty=" + difficulty + "]";
	}
	
}
